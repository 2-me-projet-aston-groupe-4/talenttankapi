<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class EmployerJWTMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $userRole = auth()->user()->role;

            if($user && $userRole == 'admin'){
                return $next($request);
            } else if($user && $userRole == 'freelancer'){
                return response()->json(['error' => 'Unauthiorzed'], 401);
            } else if($user && $userRole == 'employer'){
                return $next($request);
            } 
        } catch(Exception $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['error' => 'Token is invalid'], 401);
            } else {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        }
    }
}
