<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class AuthController extends Controller
{
    /**
     * Register a new user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if ($request->firstname == null or $request->lastname == null) {
            // #### FIX #### error code 401 ? needs to be corrected
            return response()->json(['error' => 'firstname and lastname cannot be null'], 401);
        } else {
            $user = User::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'password' => $request->password,
                'email' => $request->email,
                'phone' => $request->phone,
                'role' => $request->role
            ]);

            $token = auth()->login($user);
            return $this->getToken($token);
        }
    }

    /**
     * Login
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'E-mail/password incorrecte']);
        } elseif (auth()->user()->is_email_confirmed == false) {
            return response()->json(['error' => 'Email not verified']);
        }
        return $this->getToken($token);

    }

    /**
     * Logout
     *
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get token
     *
     */
    protected function getToken($token)
    {
        $data = ['user' => auth()->user()];
        $customClaims = auth()->factory()->customClaims($data);
        $payload = auth()->factory()->make($data);
        $token = JWTAuth::encode($payload);

        return response()->json([
            'access_token' => $token->get(),
            'token_type' => 'bearer',
        ]);
    }

}
