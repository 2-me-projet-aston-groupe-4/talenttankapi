<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        // $user 
        // $users = User::with('address')->get();
        // $users = DB::table('user')
        //     ->join('address', 'user.iduser', '=', 'address.user_idUser')
        //     ->get();

        //return response()->json(['users' => $users]);
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = auth()->user()->iduser;

        if($user_id == $id ) {
            $user = User::findOrFail($id);
            return response()->json(['user' => $user]);
        } else {
            return response()->json(['error' => 'User not found'], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == Auth::id()) {
            $user = Auth::user();

            $address = DB::select('
                select *
                from address
                where user_idUser = ?',
                [$id]
            );
            $address = $address[0];

            $firstname =  $request->firstname ? $request->firstname : $user->firstname;
            $lastname =  $request->lastname ? $request->lastname : $user->lastname;
            $phone =  $request->phone ? $request->phone : $user->phone;

            $street =  $request->street ? $request->street : $address->street;
            $city =  $request->city ? $request->city : $address->city;
            $postal_code =  $request->postal_code ? $request->postal_code : $address->postal_code;
            $longitude =  $request->longitude ? $request->longitude : $address->longitude;
            $latitude =  $request->latitude ? $request->latitude : $address->latitude;
            $country =  $request->country ? $request->country : $address->country;
            

            DB::table('user')->where('iduser', $id)->update([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'phone' => $phone
            ]);
            DB::table('address')->where('user_idUser',$id)->update([
                'street' => $street,
                'city' => $city,
                'postal_code' => $postal_code,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'country' => $country,
            ]);
            return response()->json(['user_update' => 'user updated successfully']);
        }
        return response()->json(['error' => 'Unauthorized']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }

    /**
     * Validate a user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateUser($id)
    {
        $user = User::findOrFail($id);
        if($user->is_email_confirmed) {
            $user->is_user_active = true;
            $user->save();

            return response()->json(['message' => 'User validated'], 201);
        } else {
            return response()->json(['message' => 'User\'s email not confirmed']);
        }
    }


    public function getAllFreelanceAvailableToWork() {
        $freelance = DB::table('user')->where('is_user_able_to_work', '1')->get();
        return response()->json(['freelance' => $freelance]);
    }

    public function validateFreelanceForMission( $idFreelance, $idPro, $idMission) {

        // MAJ champs disponibilité de la mission et id_freelance assigné à la mission 
        $mission = DB::table('mission')->where('id_professionnal_creator', $idPro)->get();

        if ($mission[0]->idmission == $idMission) {
            $mission = DB::table('mission')
            ->where('id_professionnal_creator', $idPro)
            ->where('idmission', $idMission)
            ->update(
                [
                    'is_available' => '0',
                    'id_freelance_accepted' => $idFreelance
                ]
            );
        } else {
            return response()->json(['error' => "Cette mission n'appartient pas à ce professionnel."]);
        }

        // MAJ champ disponibilité du freelance
        $freelance = DB::table('user')
        ->where('iduser', $idFreelance)
        ->update(['is_user_able_to_work' => '0']);

        // Suppression des candidatures faites par le freelance
        $candidature = DB::table('user_has_postuled_mission')
        ->where('id_freelance', $idFreelance)
        ->where('id_mission', '!=', $idMission)
        ->delete();

        return response()->json(['validation' => "Le freelance est assigné à la mission."]);
    }
}
