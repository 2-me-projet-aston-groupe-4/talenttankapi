<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use ModelNotFoundException;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MissionController extends Controller
{
    public function getOneMission($idMission) {
        $idExist = false;
        if ($idMission) {
            $listIdMissions = DB::table('mission')->select('idmission')->get();
            for ($i = 0; $i < count($listIdMissions); $i++) {
                $currentIdMission =  $listIdMissions[$i]->idmission;
                if ($idMission ==  $currentIdMission) {
                    $idExist = true;
                }
            };
            if ($idExist) {
                $mission = DB::table('mission')
                ->join('user', 'id_professionnal_creator', 'user.iduser')
                ->where('idmission', $idMission)
                ->first();

                $categories = DB::table('category')
                ->select('category.label', 'idmission')
                ->join('mission_has_category', 'idCategory', 'category_idCategory')
                ->join('mission', 'idmission', 'mission_idMission')
                ->where('idmission', $idMission)
                ->get();

                $userAcceptedJson = DB::table('user')
                ->select('firstname', 'lastname')
                ->join('mission', 'iduser', 'id_freelance_accepted')
                ->where('idmission', $idMission)
                ->first();

                $userAccepted = "";
                if (isset($userAcceptedJson->firstname) && isset($userAcceptedJson->lastname)) {
                    $userAccepted = $userAcceptedJson->firstname . " " . $userAcceptedJson->lastname;
                }

                $usersPostuledJson = DB::table('user')
                ->join('user_has_postuled_mission', 'id_freelance', 'iduser')
                ->join('mission', 'idmission', 'id_mission')
                ->where('idmission', $idMission)
                ->get();

                $userPostuled = [];
                if (count($usersPostuledJson) > 0) {
                    for ($i = 0; $i < count($usersPostuledJson); $i++) {
                        $arrayProposalDate = explode("-", $usersPostuledJson[$i]->proposal_date);
                        $newProposalDate = $arrayProposalDate[2] . '/' . $arrayProposalDate[1] . '/' . $arrayProposalDate[0];
                        array_push($userPostuled, [
                                "id" => $usersPostuledJson[$i]->iduser,
                                "name" => $usersPostuledJson[$i]->firstname . " " . $usersPostuledJson[$i]->lastname,
                                "proposalPrice" => $usersPostuledJson[$i]->proposal_price,
                                "proposalDate" => $newProposalDate
                            ]
                        );
                    };
                };

                $result = [];
                $arrayDate = explode("-", $mission->creation_date);
                $newDate = $arrayDate[2] . '/' . $arrayDate[1] . '/' . $arrayDate[0];
                $newIsAvailable = false;
                if ($mission->is_available == 1) {
                    $newIsAvailable = true;
                }
                array_push($result, [
                        "idMission" => $mission->idmission,
                        "label" => $mission->label,
                        "author" => $mission->firstname . " " . $mission->lastname,
                        "description" => $mission->description,
                        "isAvailable" => $newIsAvailable,
                        "creationDate" => $newDate,
                        "duration" => $mission->duration,
                        "budget" => $mission->budget,
                        "categories" => $categories,
                        "userAccepted" => $userAccepted,
                        "userPostuled" => $userPostuled
                    ]
                );
                return response()->json($result);
            } else {
                return response()->json("Cette mission n'existe pas");
            }
        } else {
            return response()->json("L'id de la mission n'est pas renseigné");
        }
    }

    public function getAllMissions() {
        $missions = DB::table('mission')
        ->join('user', 'id_professionnal_creator', 'user.iduser')
        ->get();
        $result = [];
        for ($i = 0; $i < count($missions); $i++) {
            $arrayDate = explode("-", $missions[$i]->creation_date);
            $newDate = $arrayDate[2] . '/' . $arrayDate[1] . '/' . $arrayDate[0];
            $newIsAvailable = false;
            if ($missions[$i]->is_available == 1) {
                $newIsAvailable = true;
            }
            
            $categories = DB::table('category')
            ->select('category.label')
            ->join('mission_has_category', 'idCategory', 'category_idCategory')
            ->join('mission', 'idmission', 'mission_idMission')
            ->where('idmission', $missions[$i]->idmission)
            ->get();

            $usersPostuledJson = DB::table('user')
            ->join('user_has_postuled_mission', 'id_freelance', 'iduser')
            ->join('mission', 'idmission', 'id_mission')
            ->where('idmission', $missions[$i]->idmission)
            ->get();

            $userPostuled = [];
            if (count($usersPostuledJson) > 0) {
                for ($j = 0; $j < count($usersPostuledJson); $j++) {
                    array_push($userPostuled, [
                            "id" => $usersPostuledJson[$j]->iduser,
                            "name" => $usersPostuledJson[$j]->firstname . " " . $usersPostuledJson[$j]->lastname,
                        ]
                    );
                };
            }

            array_push($result, [
                "idMission" => $missions[$i]->idmission,
                "label" => $missions[$i]->label,
                "author" => $missions[$i]->firstname . " " . $missions[$i]->lastname,
                "isAvailable" => $newIsAvailable,
                "creationDate" => $newDate,
                "duration" => $missions[$i]->duration,
                "budget" => $missions[$i]->budget,
                "categories" => $categories,
                "userPostuled" => $userPostuled
                ]
            );
        }
        return response()->json($result);
    }

    public function getAllMissionDoingByFreelance($idUser) {
        $idExist = false;
        if ($idUser) {
            $listIdUser = DB::table('user')->select('iduser')->get();
            for ($i = 0; $i < count($listIdUser); $i++) {
                $currentIdUser =  $listIdUser[$i]->iduser;
                if ($idUser ==  $currentIdUser) {
                    $idExist = true;
                }
            };
            if ($idExist) {
                $missions = DB::table('mission')->where('id_freelance_accepted', $idUser)->get();
                return response()->json(['missions' => $missions]);
            } else {
                return response()->json(['error' => "Cette utilisateur n'existe pas"]);
            }
        } else {
            return response()->json(['error' => "L'id de la mission n'est pas renseigné"]);
        }
    }

    public function getAllMissionCandidatedByFreelance($idUser) {
        $missions = DB::table('user')
        ->join('user_has_postuled_mission', 'iduser', 'user_has_postuled_mission.id_freelance')
        ->join('mission', 'idmission', 'user_has_postuled_mission.id_mission')
        ->select('iduser', 'firstname', 'lastname', 'idmission')
        ->where('user.iduser', $idUser)
        ->get();
        return response()->json(['missions' => $missions]);
    }

    public function getAllFreelanceCandidatedMission($idMission) {
        $missions = DB::table('mission')
        ->join('user_has_postuled_mission', 'idmission', 'user_has_postuled_mission.id_mission')
        ->join('user', 'iduser', 'user_has_postuled_mission.id_freelance')
        ->select('iduser', 'firstname', 'lastname', 'idmission')
        ->where('mission.idmission', $idMission)
        ->get();
        return response()->json(['missions' => $missions]);
    }

    public function getAllMissionCreatedByProfessionnal($idUser) {
        $missions = DB::table('mission')->where('id_professionnal_creator', $idUser)->get();
        $result = [];
        for ($i = 0; $i < count($missions); $i++) {
            $arrayDate = explode("-", $missions[$i]->creation_date);
            $newDate = $arrayDate[2] . '/' . $arrayDate[1] . '/' . $arrayDate[0];
            $newIsAvailable = false;
            if ($missions[$i]->is_available == 1) {
                $newIsAvailable = true;
            }
            
            $categories = DB::table('category')
            ->select('category.label')
            ->join('mission_has_category', 'idCategory', 'category_idCategory')
            ->join('mission', 'idmission', 'mission_idMission')
            ->where('idmission', $missions[$i]->idmission)
            ->get();

            $userAcceptedJson = DB::table('user')
            ->select('firstname', 'lastname')
            ->join('mission', 'iduser', 'id_freelance_accepted')
            ->where('idmission', $missions[$i]->idmission)
            ->first();

            $userAccepted = "";
            if (isset($userAcceptedJson->firstname) && isset($userAcceptedJson->lastname)) {
                $userAccepted = $userAcceptedJson->firstname . " " . $userAcceptedJson->lastname;
            };
            
            $usersPostuledJson = DB::table('user')
            ->join('user_has_postuled_mission', 'id_freelance', 'iduser')
            ->join('mission', 'idmission', 'id_mission')
            ->where('idmission', $missions[$i]->idmission)
            ->get();

            $userPostuled = [];
            if (count($usersPostuledJson) > 0) {
                for ($j = 0; $j < count($usersPostuledJson); $j++) {
                    array_push($userPostuled, [
                            "id" => $usersPostuledJson[$j]->iduser,
                            "name" => $usersPostuledJson[$j]->firstname . " " . $usersPostuledJson[$j]->lastname,
                        ]
                    );
                };
            }

            array_push($result, [
                "idMission" => $missions[$i]->idmission,
                "label" => $missions[$i]->label,
                "isAvailable" => $newIsAvailable,
                "creationDate" => $newDate,
                "duration" => $missions[$i]->duration,
                "budget" => $missions[$i]->budget,
                "categories" => $categories,
                "userAccepted" => $userAccepted,
                "userPostuled" => $userPostuled
                ]
            );
        }
        return response()->json($result);
    }

    public function getAllMissionsAvailable() {
        $missions = DB::table('mission')
        ->where('is_available', '1')
        ->get();
        return response()->json(['missions' => $missions]);
    }

    public function getMissionWithFilter(
        $available,
        $operatorDate,
        $creationDate,
        $operatorDuration,
        $duration,
        $operatorBudget,
        $budget,
        $idCategory
    ) {
        $conditions = [];

        if ($available != 'null') {
            array_push($conditions, ['mission.is_available', '=', $available]);
        }
        if ($operatorDate != 'null' && $creationDate != 'null') {
             array_push($conditions, ['mission.creation_date', $operatorDate, $creationDate]);
        }
        if ($operatorDuration != 'null' && $duration != 'null') {
            array_push($conditions, ['mission.duration','=', $duration]);
        }
        if ($operatorBudget != 'null' && $budget != 'null') {
            array_push($conditions, ['mission.budget', $operatorBudget, $budget]);
        }
        if ($idCategory != 'null') {
            array_push($conditions, ['category.idCategory', '=', $idCategory]);
        }

        $missions = DB::table('mission')
        ->join('mission_has_category', 'idmission', 'mission_has_category.mission_idMission')
        ->join('category', 'idCategory', 'mission_has_category.category_idCategory')
        ->select('idCategory', 'category.label as labelCategory', 'category.description as descriptionCategory', 'mission.*')
        ->where($conditions)
        ->get();

        return response()->json(['missions' => $missions]);
    }

    public function createMission(Request $request) {
        $label = $request->label;
        $description = $request->description;
        $isAvailable = 0;
        $currentDate = date('Y-m-d');
        $duration = $request->duration;
        $budget = $request->budget;
        $idProfessionnal = $request->idProfessionnalCreator;
        $idFreelance = null;
        $categories = $request->categories;

        $createMission = DB::table('mission')
        ->insert(
            [
                'label' => $label,
                'description' => $description,
                'is_available' => 1,
                'creation_date' => $currentDate,
                'duration' => $duration,
                'budget' => $budget,
                'id_professionnal_creator' => $idProfessionnal,
                'id_freelance_accepted' => null
            ]
/*             [
                'label' => $label,
                'description' => "",
                'is_available' => 0,
                'creation_date' => $currentDate,
                'duration' => 10,
                'budget' => 100,
                'id_professionnal_creator' => 7,
                'id_freelance_accepted' => null
            ] */
        );

        
        $listMission = DB::table('mission')->select('idmission')->get();
        $idCurrentMission = $listMission[count($listMission) - 1]->idmission;        

        for ($i = 0; $i < count($categories); $i++) {
            print_r($categories);
            print_r($categories[$i]["id"]);
            $linkCategoriesForMission = DB::table('mission_has_category')
            ->insert(
                [
                    "mission_idMission" => $idCurrentMission,
                    "category_idCategory" => $categories[$i]["id"]
                ]
            );
        }

        return response()->json(['creation' => 'Mission créée']);
    }

    public function candidate(Request $request) {
        $idFreelance = $request->idFreelance;
        $idMission = $request->idMission;
        $proposalPrice = $request->proposalPrice;
        //$proposalDate = $request->proposalDate;
        $currentDate = date('Y-m-d');

/*         $mission = DB::table('mission')->where('idmission', $idMission)->get();
        if ($mission[0]->is_available == 0) {
            return response()->json(['error' => "Cette mission n'est plus ouverte à candidature"]);
        }

        $candidature = DB::table('user_has_postuled_mission')
        ->where('id_freelance', $idFreelance)
        ->where('id_mission', $idMission)
        ->get();

        if (count($candidature) != 0) {
            return response()->json(['error' => "Cet utilisateur a déjà postulé à cette mission."]);
        } */
        
/*         if( $proposal_price == '') {
            $proposal_price = null;
        };
        if ($proposal_date == '') {
            $proposal_date = null;
        }; */

        $createMission = DB::table('user_has_postuled_mission')
        ->insert(
/*             [
                'id_freelance' => $idFreelance,
                'id_mission' => $idMission,
                'proposal_price' => $proposalPrice,
                'status' => '?',
                'proposal_date' => $currentDate,
            ] */
            [
                'id_freelance' => $idFreelance,
                'id_mission' => $idMission,
                'proposal_price' => $proposalPrice,
                'status' => '?',
                'proposal_date' => $currentDate,
            ]
        );
        return response()->json(['candidature' => 'Candidature créée']);
    }
    
    public function updateMission(Request $request, $idMission) {
        $label = $request->label;
        $description = $request->description;
        $isAvailable = $request->is_available;
        $creationDate = $request->creation_date;
        $duration = $request->duration;
        $budget = $request->budget;
        $idProfessionnal = $request->id_professionnal_creator;
        $idFreelance = $request->id_freelance_accepted;

        $updateMission = DB::table('mission')
        ->where('idmission', $idMission)
        ->update(
            [
                'label' => $label,
                'description' => $description,
                'is_available' => $isAvailable,
                'creation_date' => $creationDate,
                'duration' => $duration,
                'budget' => $budget,
                'id_professionnal_creator' => $idProfessionnal,
                'id_freelance_accepted' => $idFreelance
            ]
        );
        return response()->json(['MAJ' => 'Mission mise à jour']);
    }

    public function activateMission($idMission) {
        $updateMission = DB::table('mission')
        ->where('idmission', $idMission)
        ->update(['is_available' => '1']);
        return response()->json(['activation' => 'Mission activée']);
    }

    public function desactivateMission($idMission) {
        $updateMission = DB::table('mission')
        ->where('idmission', $idMission)
        ->update(['is_available' => '0']);
        return response()->json(['desactivation' => 'Mission désactivée']);
    }

    public function deleteMission($idMission) {
        $deleteCategoryMission = DB::table('mission_has_category')
        ->where('mission_idMission', $idMission)
        ->delete();

        $deleteUserPostuledMission = DB::table('user_has_postuled_mission')
        ->where('id_mission', $idMission)
        ->delete();

        $deletedMission = DB::table('mission')
        ->where('idmission', $idMission)
        ->delete();

        return response()->json(['suppression' => 'Mission supprimée']);
    }

    /**
     * Freelancer: Confirm freelance application
     *
     * @param  int  $id_freelance 
     * @param  int  $id_mission
     * @return \Illuminate\Http\Response
     */
    public function confirmApplication($id_freelance, $id_mission)
    {
        if($id_freelance == Auth::id()) {
            $applicationStatus = DB::select('
                select status
                from user_has_mission
                where id_freelance = ?
                and id_mission = ?',
                [$id_freelance, $id_mission]
            );

            if($applicationStatus && $applicationStatus[0]->status == 0){
                DB::table('user_has_mission')->update(['status' => 1]);
                return response()->json(['application' => 'Confirmed']);
            } else if($applicationStatus && $applicationStatus[0]->status == 1){
                return response()->json(['application' => 'Already confirmed']);
            }
            return response()->json(['error' => 'Application not founded']);
        }
        return response()->json(['error' => 'Unauthorized']); 
    }
}
