<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {    
    return $request->user();
});

/*
* Unprotected Routes
*
*/

// AJOUTER ICI POUR TEST ANDROID à remettre avec jwt.admin.verify
Route::get('/user/getAll', 'UserController@index');

Route::post('/register', 'AuthController@register');

Route::post('/login', 'AuthController@login');

Route::post('/logout', 'AutController@logout');

Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');




//------------------------------------------------------------------MISSION------------------------------------------------------------------------//

//---------------Unprotected Routes Mission

// obtenir toutes les missions existantes
Route::get('/missions', 'MissionController@getAllMissions');
// obtenir toutes les missions disponible
Route::get('/availableMission', 'MissionController@getAllMissionsAvailable');
// obtenir toutes les missions en fonction des filtres : disponinilité, date, durée, prix et catégorie 
Route::get('/filteredMission/{available}/{operatorDate}/{creationDate}/{operatorDuration}/{duration}/{operatorBudget}/{budget}/{category}', 'MissionController@getMissionWithFilter');

//---------------Protected Routes Mission

// obtenir le détail d'une mission
Route::get('/missions/{idMission}', 'MissionController@getOneMission');
// obtenir toutes les missions acceptés d'un freelance
Route::get('/missions/freelance/accepted/{idUser}', 'MissionController@getAllMissionDoingByFreelance');
// obtenir toutes les missions postulés par un freelance
Route::get('/missions/freelance/candidated/{idUser}', 'MissionController@getAllMissionCandidatedByFreelance');
// obtenir toutes les freelance ayant postulé à une mission
Route::get('/missions/freelance/{idMission}', 'MissionController@getAllFreelanceCandidatedMission');
// candidater à une mission pour un freelance
Route::post('/missions/freelance/candidate', 'MissionController@candidate');
// obtenir toutes les missions liées à un professionnel
Route::get('/missions/pro/{idUser}', 'MissionController@getAllMissionCreatedByProfessionnal');
// creation d'une mission par un professionnel/admin
Route::post('/missions/pro/creation', 'MissionController@createMission');
// modification d'une mission par un professionnel/admin
Route::put('/missions/pro/update/{idMission}', 'MissionController@updateMission');
// désactiver une mission par un professionnel/admin
Route::patch('/missions/pro/desactivate/{idMission}', 'MissionController@desactivateMission');
// activer une mission par un professionnel/admin
Route::patch('/missions/pro/activate/{idMission}', 'MissionController@activateMission');
// suppression d'une mission par un professionnel/admin
Route::delete('/missions/pro/delete/{idMission}', 'MissionController@deleteMission');

//-------------------------------------------------------------------------FREELANCE------------------------------------------------------------------------//

//------------Protected


//------------Unprotected
// obtenir tous les freelances dispobiles
Route::get('/freelance/available', 'UserController@getAllFreelanceAvailableToWork');

//-------------------------------------------------------------------------PROFESSIONNAL------------------------------------------------------------------------//

//------------Protected


//------------Unprotected
// valider un freelance pour une mission
Route::patch('/pro/acceptFreelance/{idFreelance}/{idPro}/{idMission}', 'UserController@validateFreelanceForMission');

//------------------------------------------------------------------------------------------------------------------------------------------//
/*
* ADMIN Side protected routes
*
*/

Route::group(['middleware' => ['jwt.admin.verify']], function() {
    
    Route::delete('/user/delete/{user}', 'UserController@destroy');
    Route::put('/user/validateUser/{id}', 'UserController@validateUser');
});

/*
* FREELANCER Side protected routes
*
*/

Route::group(['middleware' => ['jwt.freelancer.verify']], function() {
    //User
    Route::get('/user/getUser/{id}', 'UserController@show');
    Route::post('/user/update/{id}', 'UserController@update');
    Route::post('/mission/freelance/confirmApplication/{id_freelancer}/{id_mission}', 'MissionController@confirmApplication');
});

/*
* EMPLOYER Side protected routes
*
*/

Route::group(['middleware' => ['jwt.employer.verify']], function() {
    Route::post('/user/update/{id}', 'UserController@update');
});