INSERT INTO talent_tank.user VALUES (1, 'John', 'Lennon', 'john', 'john.lennon@gmail.com', '0673628139', false, true, true, 'freelance');
INSERT INTO talent_tank.user VALUES (2, 'Bob', 'Dylan', 'bob', 'bob.dylan@gmail.com', '0665211722', true, true, true, 'professionnal');
INSERT INTO talent_tank.user VALUES (3, 'Bob', 'Marley', 'bobby', 'bob.marley@gmail.com', '0685211722', false, true, true, 'freelance');
INSERT INTO talent_tank.user VALUES (4, 'Lionel', 'Messi', 'leo', 'lionel.messi@gmail.com', '0665217822', true, true, true, 'professionnal');
INSERT INTO talent_tank.user VALUES (5, 'Serge', 'Gainsbourg', 'serge', 'serge.gainsbourg@gmail.com', '0665212322', true, true, true, 'freelance');

INSERT INTO talent_tank.mission VALUES (1, 'Developpeur React', 'React confirrmé', false, '2020/08/24', 10, 1500.00, 2, 1);
INSERT INTO talent_tank.mission VALUES (2, 'Developpeur PHP', 'PHP expert', true, '2020/08/25', 20, 3500.00, 4, null);
INSERT INTO talent_tank.mission VALUES (3, 'Administrateur Systeme', 'Débutant', true, '2020/08/08', 25, 3000.00, 4, null);
INSERT INTO talent_tank.mission VALUES (4, 'Developpeur React', 'Junior', false, '2020/08/12', 10, 1000.00, 2, 3);
INSERT INTO talent_tank.mission VALUES (5, 'DevOps', 'Expert', true, '2020/08/10', 10, 4000.00, 4, null);

INSERT INTO talent_tank.user_has_postuled_mission VALUES (1, 2, null, null, null);
INSERT INTO talent_tank.user_has_postuled_mission VALUES (1, 3, null, null, null);
INSERT INTO talent_tank.user_has_postuled_mission VALUES (3, 2, null, null, null);
INSERT INTO talent_tank.user_has_postuled_mission VALUES (5, 2, null, null, null);
INSERT INTO talent_tank.user_has_postuled_mission VALUES (5, 3, null, null, null);

INSERT INTO talent_tank.category VALUES (1, 'Front', 'aaaa');
INSERT INTO talent_tank.category VALUES (2, 'Back', 'bbbb');
INSERT INTO talent_tank.category VALUES (3, 'Fullstack', 'cccc');
INSERT INTO talent_tank.category VALUES (4, 'Admin-System', 'dddd');
INSERT INTO talent_tank.category VALUES (5, 'DevOps', 'eeee');

INSERT INTO talent_tank.mission_has_category VALUES (1, 1);
INSERT INTO talent_tank.mission_has_category VALUES (2, 1);
INSERT INTO talent_tank.mission_has_category VALUES (3, 2);
INSERT INTO talent_tank.mission_has_category VALUES (4, 4);
INSERT INTO talent_tank.mission_has_category VALUES (5, 5);

{
    "firstname": "kevin",
    "lastname": "pemonon",
    "password": "pempem",
    "email": "test@gmail.com",
    "phone": "0673628139",
    "role": "freelance"
}

{
    "firstname": "kevin",
    "lastname": "pemonon",
    "password": "pempem",
    "email": "test@gmail.com",
    "phone": "0673628139",
    "role": "professionnal"
}

{
    "email": "john.lennon@gmail.com",
    "password": "pempem"
}